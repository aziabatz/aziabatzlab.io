---
home: true
title: Home
heroImage: /hero.png

actionText: See my Github
actionLink: https://github.com/aziabatz

xfeatures: 
- title: C++
  details: https://user-images.githubusercontent.com/42747200/46140125-da084900-c26d-11e8-8ea7-c45ae6306309.png
  link: https://en.wikipedia.org/wiki/C%2B%2B
- title: GNU Assembler
  details: https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/Heckert_GNU_white.svg/500px-Heckert_GNU_white.svg.png
- title: Java(SE & EE)
  details: https://images.vexels.com/media/users/3/166401/isolated/preview/b82aa7ac3f736dd78570dd3fa3fa9e24-java-programming-language-icon.png
- title: Kotlin
  details: https://upload.wikimedia.org/wikipedia/commons/thumb/0/06/Kotlin_Icon.svg/2048px-Kotlin_Icon.svg.png
- title: Python
  details: https://cdn3.iconfinder.com/data/icons/logos-and-brands-adobe/512/267_Python-512.png
- title: Dart
  details: https://codersdaily.in/media/courses/dartlogo.png
- title: Bash
  details: https://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Bash_Logo_Colored.svg/1200px-Bash_Logo_Colored.svg.png
- title: HTML5
  details: https://www.w3.org/html/logo/downloads/HTML5_Badge_512.png
- title: CSS3
  details: https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/CSS3_logo.svg/800px-CSS3_logo.svg.png
- title: Javascript
  details: https://upload.wikimedia.org/wikipedia/commons/6/6a/JavaScript-logo.png
- title: Typescript
  details: https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Typescript_logo_2020.svg/1024px-Typescript_logo_2020.svg.png
- title: SQL
  details: https://upload.wikimedia.org/wikipedia/fr/thumb/6/68/Oracle_SQL_Developer_logo.svg/langfr-220px-Oracle_SQL_Developer_logo.svg.png
- title: MongoDB
  details: https://victorroblesweb.es/wp-content/uploads/2016/11/mongodb.png
  
footer: Copyright © 2022 (MIT Licensed) Ahmed Ziabat
---


<div class="features">
  <div class="feature" v-for="feat in $page.frontmatter.xfeatures">
    <img style="display:block;margin:auto;max-height:150px;max-width:200px;height:auto;width:auto" v-bind:src="feat.details"/>
    <h2 style="text-align: center">{{ feat.title }}</h2>
  </div>
</div>  

